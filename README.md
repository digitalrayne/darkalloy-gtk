# darkalloy-gtk

![Latest release](https://gitlab.com/digitalrayne/darkalloy-gtk/-/badges/release.svg)

![DarkAlloy theme logo](logo.png)

This is a repackaging of the DarkAlloy GTK (GTK 2.0 currently) theme by DarkAlloy.

This is assembled from the most recent versions of the theme files I could find in various archives, as DarkAlloy no longer seems to have a web presence.

## Purpose

I (Rayne) have been using this theme (and GTK) since I was a teenager, and I wanted to preserve and make available packaged versions of these themes. Having the theme in a `git` repository means I can set up CI and release tarballs, which makes it easier to package for usage in different OSes which still package GTK.

If you find any issues with this theme feel free to open up an issue.

## GTK and WM versions

These theme files currently cover:
 - GTK 2.0
 - Metacity
 
I am working on porting to GTK versions 3 & 4 (which use CSS, and deprecate the pixmap engine).

I also aim to add support for WMs like Mutter/Muffin/XFWM4 so this can be used with Cinnamon, XFCE, GNOME, etc.

## Releases

[Releases](https://gitlab.com/digitalrayne/darkalloy-gtk/-/releases) track the tags in this repository, and follow semantic versioning.

The main branch is used for development, and I cut a release when there are changes.

GitLab releases are used, 

## Theme Description

This is the DarkAlloy theme for Enlightenment DR-0.16.

It was created, and is maintained, by DarkAlloy.

## Original Links

Preserved for attribution and historical reasons.

DarkAlloy's email: scarab@egyptian.net

Original Homepages:
 - http://www.egyptian.net/~scarab
 - http://sublevel27.com/cricketwings
 - http://darkalloy.structum.com.mx
